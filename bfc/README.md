## [边距重叠](./demo01.html)

两个或多个盒子(**可能相邻也可能嵌套**)的相邻边界(其间没有任何非空内容、补白、边框)重合在一起而形成一个单一边界。使用bfc方法来实现包裹,会把超出的边距也计算进去

原因是如果块元素的 `margin-top` 与它的第一个子元素的 `margin-top` 之间没有 `border`、`padding`、`inline` `content`、 `clearance` 来分隔，或者块元素的 margin-bottom 与它的最后一个子元素的 margin-bottom 之间没有 `border`、`padding`、`inline` `content`、`height`、`min-height`、 `max-height` 分隔，那么外边距会塌陷。子元素多余的外边距会被父元素的外边距截断



## [解决文字环绕图片](./demo02.html)

* 解决文字环绕图片；左边图片div，右边文本容器p，将p容器开启bfc；